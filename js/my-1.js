import { arrayTen } from './arrayData.js';

function selectionSort(arr) {
	let result = [...arr];
	for (let i = 0; i < result.length; i++) {
		let indexMin = i;

		// console.group('Отсортированное / Не отсортированное');
		// console.log(result.slice(0, i));
		// console.log(result.slice(i));
		// console.groupEnd();

		for (let j = i + 1; j < result.length; j++) {
			if (result[indexMin] > result[j]) {
				indexMin = j;
			}
		}

		if (result[i] > result[indexMin]) {
			let temp = result[i];
			result[i] = result[indexMin];
			result[indexMin] = temp;
		}
	}

	return result;
}

const result = selectionSort(arrayTen);

console.log(result);
