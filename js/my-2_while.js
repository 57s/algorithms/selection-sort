import { array } from './arrayData.js';

function selectionSort(arr) {
	let left = 0;
	let right = arr.length - 1;

	while (left < right) {
		let minIndex = left;
		let maxIndex = right;
		let index = left;

		while (index <= right) {
			if (arr[index] < arr[minIndex]) minIndex = index;
			if (arr[index] > arr[maxIndex]) maxIndex = index;

			index++;
		}

		if (minIndex !== left) {
			[arr[minIndex], arr[left]] = [arr[left], arr[minIndex]];
		}

		if (maxIndex !== right) {
			[arr[maxIndex], arr[right]] = [arr[right], arr[maxIndex]];
		}

		left++;
		right--;
	}

	return arr;
}

const result = selectionSort(array);

console.log(result);
