import { arraySmall } from './arrayData.js';

function selectionSort(arr) {
	let result = [...arr];

	for (let i = result.length - 1; i >= 0; i--) {
		let maxIndex = i;

		for (let j = i - 1; j >= 0; j--) {
			if (result[maxIndex] < result[j]) maxIndex = j;
		}

		if (maxIndex != i) {
			let temp = result[i];
			result[i] = result[maxIndex];
			result[maxIndex] = temp;
		}
	}

	return result;
}

const result = selectionSort(arraySmall);

console.log(result);
