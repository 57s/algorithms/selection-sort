// Найти  энный наименьший и наибольший элемент
import { array } from './arrayData.js';

function selectionSort(arr, position = 1) {
	let result = [...arr];
	for (let i = 0; i < position; i++) {
		let indexMin = i;
		let indexMax = i;

		for (let j = i + 1; j < result.length - i; j++) {
			if (result[indexMin] > result[j]) indexMin = j;
			if (result[indexMax] < result[j]) indexMax = j;
		}

		// Фикс. Что бы при массиве длинной в два элемента не сортировалось в обратную сторону
		if (indexMin !== i && result.length > 2) {
			let temp = result[i];
			result[i] = result[indexMin];
			result[indexMin] = temp;
		}

		if (indexMax !== result.length - i - 1) {
			let temp = result[result.length - i - 1];
			result[result.length - i - 1] = result[indexMax];
			result[indexMax] = temp;
		}
	}

	return [result[position - 1], result[result.length - position]];
}

const result = selectionSort(array, 2);

console.log(result);
