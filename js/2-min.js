//  От min до max
import { arrayTen } from './arrayData.js';

function selectionSort(arr) {
	for (let j = 0; j < arr.length - 1; j++) {
		let min = Infinity;
		let index = null;

		for (let i = j; i < arr.length; i++) {
			if (arr[i] < min) {
				min = arr[i];
				index = i;
			}
		}

		const buff = arr[j];
		arr[j] = min;
		arr[index] = buff;
	}

	return arr;
}

const result = selectionSort(arrayTen);

console.log(result);
