// Найти наименьший и наибольший элемент

import { arrayRandom } from './arrayData.js';

function searchMinMax(arr) {
	let max = arr.at(0);
	let min = arr.at(0);

	for (let i = 0; i < arr.length; i++) {
		if (max < arr[i]) max = arr[i];
		if (min > arr[i]) min = arr[i];
	}

	return [min, max];
}

let result = searchMinMax(arrayRandom);

console.log(result);

console.log('Проверка = ', [
	Math.min(...arrayRandom),
	Math.max(...arrayRandom),
]);
