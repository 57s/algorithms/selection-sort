import { array } from './arrayData.js';

function selectionSort(arr) {
	for (let i = 0; i < arr.length; i++) {
		const left = i;
		const right = arr.length - 1 - i;
		let min = left;
		let max = right;

		for (let j = left + 1; j <= right; j++) {
			if (arr[min] > arr[j]) min = j;
			if (arr[max] < arr[j]) max = j;
		}

		if (left !== min) {
			let temp = arr[left];
			arr[left] = arr[min];
			arr[min] = temp;
		}

		if (right !== max) {
			let temp = arr[right];
			arr[right] = arr[max];
			arr[max] = temp;
		}
	}
	return arr;
}

const result = selectionSort(array);

console.log(result);
