// Найти  энный наименьший и наибольший элемент
import { array } from './arrayData.js';

function selectionSort(arr, position = 1) {
	if (arr.length - 1 < position)
		return 'позиция должна быть больше длинны массива';
	let result = [...arr];
	// Делим на `2` так как сортировка идет с двух краев
	// Вычитаем `position` так как нужно энные элементы по краям
	// Вычитаем `1` так как length ссылается за пределы массива -1 = последний элемент
	for (let i = 0; i < result.length / 2 - position - 1; i++) {
		// Первый индекс после последнего отсортированного минимального элемента(с лева)
		let left = i;
		// Первый индекс после последнего отсортированного максимального элемента(с права)
		let right = result.length - i - 1;
		let indexMin = left;
		let indexMax = right;

		// `i + 1` Что бы не сортировать уже отсортированные минимальные значения
		// `result.length - i -1` Что бы не сортировать уже отсортированные максимальные значения
		for (let j = i + 1; j < result.length - i - 1; j++) {
			if (result[indexMin] > result[j]) indexMin = j;
			if (result[indexMax] < result[j]) indexMax = j;
		}

		if (left !== indexMin) {
			let temp = result[i];
			result[i] = result[indexMin];
			result[indexMin] = temp;
		}

		if (right !== indexMax) {
			let temp = result[right];
			result[right] = result[indexMax];
			result[indexMax] = temp;
		}
	}

	return [result.at(position - 1), result.at(-position)];
}

const result = selectionSort(array, 2);

console.log(result);

// Use Sort = проверка
function findNthMaxAndMin(arr, n) {
	if (n > arr.length) return 'n не может быть больше массива';

	let sortedArr = arr.sort((a, b) => a - b);
	let max = sortedArr[arr.length - n];
	let min = sortedArr[n - 1];
	return [min, max];
}

console.log(findNthMaxAndMin(array, 2));
