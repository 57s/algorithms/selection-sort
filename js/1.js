import { arrayTen } from './arrayData.js';

function selectionSort(arr, isReverse = false) {
	let { length } = arr;

	for (let i = 0; i < length - 1; i++) {
		let indexMin = i;

		for (let j = i + 1; j < length; j++) {
			let condition = isReverse
				? arr[indexMin] < arr[j]
				: arr[indexMin] > arr[j];
			if (condition) {
				indexMin = j;
			}
		}

		if (arr[i] !== arr[indexMin]) {
			let temp = arr[i];
			arr[i] = arr[indexMin];
			arr[indexMin] = temp;
		}
	}

	return arr;
}

const result = selectionSort(arrayTen);

console.log(result);
