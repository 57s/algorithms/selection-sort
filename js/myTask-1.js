import { arrayDuplicates } from './arrayData.js';

function sort(arr) {
	let result = [];

	while (arr[0]) {
		let start = 0;
		let min = start;

		for (let i = 0; i < arr.length; i++) {
			if (arr[min] > arr[i]) min = i;
		}

		if (min !== start) {
			result.push(arr[min]);
			arr.splice(min, 1);
		} else {
			result.push(arr[start]);
			arr.splice(start, 1);
		}
	}

	return result;
}

const result = sort(arrayDuplicates);

console.log(result);
