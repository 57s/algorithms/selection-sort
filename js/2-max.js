//  От max до min
import { arrayTen } from './arrayData.js';

function selectionSort(arr) {
	for (let j = 0; j < arr.length - 1; j++) {
		let max = -Infinity;
		let index = null;

		for (let i = 0; i < arr.length - j; i++) {
			if (arr[i] > max) {
				max = arr[i];
				index = i;
			}
		}

		const buff = arr[arr.length - 1 - j];
		arr[arr.length - 1 - j] = max;
		arr[index] = buff;
	}

	return arr;
}

const result = selectionSort(arrayTen);

console.log(result);
